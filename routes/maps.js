module.exports=function(app){
    var authMW=require("../middleware/generic/auth");
    var renderMW=require("../middleware/generic/render");
    
    var objrep={};

    //térkép kiválasztása id alapján
    app.get('/maps/:id',
        authMW(),
        renderMW(objrep,'map'));
    
    //térkép választó oldal lekérése
    app.get('/maps',
        authMW(),
        renderMW(objrep,'maps'));
    
    //térkép hozzáadás
    app.post('/maps/add',
        authMW(),
        renderMW(objrep,'map'));
    //térkép törlése
    app.post('/maps/del/:id',
        authMW(),
        renderMW(objrep,'map'));
    //térkép mezőjének módosításának lehívása
    app.get('/maps/:id/mod/:x/:y',
        authMW(),
        renderMW(objrep,'mod'));
    //térkép mezőjének módosítása
    app.post('/maps/:id/mod/:x/:y',
        authMW(),
        renderMW(objrep,'mod'));
};

