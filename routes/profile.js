
module.exports = function(app){
    var authMW=require("../middleware/generic/auth");
    var renderMW=require("../middleware/generic/render");
    var objrep={};
    //Oldal megjelenítés
    app.get("/profile",
        authMW(),
        renderMW(objrep,'profile'));
    //jelszó módosítás
    app.post("/profile",
        authMW(),
        renderMW(objrep,'login'));
    //felhasználó törlés
    app.post("/profile/del",
        authMW(),
        renderMW(objrep,'login'));
};