module.exports=function(app){
    var authMW=require("../middleware/generic/auth");
    var renderMW=require("../middleware/generic/render");
    var objrep={};

    //Bejelentkező képernyő
    app.get('/login',
        renderMW(objrep,'login'));
    //Bejelentkezés
    app.post('/login',
        renderMW(objrep,'login'));
};