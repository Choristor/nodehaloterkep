/**
 * map model (mock)
 * @constructor
 */

 var map = function(){

 };

 /**
  * An instance
  */
 var MapInstanceMock={
     id:1,
     name: 'map name'
 };

 /**
  * find one map by id
  */
 Map.prototype.findOne = function(criteria, cb){
     return cb(null, MapInstanceMock);
 }


 /**
  * Save a map to the db
  */

Map.prototype.save=function(cb){
    return cb(null,this);
};

/**
 * Delete a map
 */
Map.prototype.delete=function(cb){
    return cb(null);
};

module.exports=Map;