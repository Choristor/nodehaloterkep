var express = require('express');
var app = express();
var bodyParser=require('body-parser');

app.use(express.static('static'));

var server = app.listen(3000,function(){
    console.log("Peace World! 3000");
});

app.use(function(req,res,next){
    res.tpl={};
    return next();
});

require("./routes/profile")(app);
require("./routes/login")(app);
require("./routes/maps")(app);


app.get('/azta',function(req,res,next){
res.end();
});

app.use(function (err,req,res,next){
    res.status(500).send("I don't have a problem but u have");
    console.error(err.stack);
});